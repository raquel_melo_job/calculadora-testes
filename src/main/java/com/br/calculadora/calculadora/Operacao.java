package com.br.calculadora.calculadora;

import java.util.Collections;
import java.util.List;

public class Operacao {

    public static int soma(int num1, int num2){
        return num1+num2;
    }

    public static int somaLista(List<Integer> numeros){
        int resultado = 0;
        for(Integer numero : numeros){
            resultado+=numero;
        }
        return resultado;
    }

    public static int subtracao(int num1, int num2){
        int resultado = 0;
        if(num1>num2){
            resultado = num1-num2;
        }else{
            resultado = num2-num1;
        }
        return resultado;
    }

    public static int subtracaoLista(List<Integer> numeros){
        Collections.sort(numeros, Collections.reverseOrder());
        int resultado = numeros.get(0);
        for(int n=1;n<numeros.size();n++){
            resultado-=n;
        }
        return resultado;
    }

    public static int multiplicacao(int num1, int num2){
        return num1*num2;
    }

    public static int multiplicacaoLista(List<Integer> numeros){
        int resultado = 1;
        for(Integer numero : numeros){
            resultado*=numero;
        }
        return resultado;
    }

    public static int divisao(int num1, int num2){
        int resultado = 0;
        if(num1>num2){
            resultado = num1/num2;
        }else{
            resultado = num2/num1;
        }
        return resultado;
    }

}
