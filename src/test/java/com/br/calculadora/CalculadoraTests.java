package com.br.calculadora;

import com.br.calculadora.calculadora.Operacao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CalculadoraTests {

    @Test
    public void testarOperaçãoDeSoma(){
        Assertions.assertEquals(Operacao.soma(1,1),2);
    }

    @Test
    public void testarOperaçãoDeSomaLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.somaLista(numeros),6);
    }

    @Test
    public void testarOperaçãoDeSubtracao(){
        Assertions.assertEquals(Operacao.subtracao(2,1),1);
    }

    @Test
    public void testarOperaçãoDeSubtracaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(5);
        Assertions.assertEquals(Operacao.subtracaoLista(numeros),2);
    }

    @Test
    public void testarOperaçãoDeMultiplicacao(){
        Assertions.assertEquals(Operacao.multiplicacao(1,2),2);
    }

    @Test
    public void testarOperaçãoDeMultiplicacaoLista(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(5);
        Assertions.assertEquals(Operacao.multiplicacaoLista(numeros),10);
    }

    @Test
    public void testarOperaçãoDeDivisao(){
        Assertions.assertEquals(Operacao.divisao(6,2),3);
    }

    @Test
    public void testarValorInvalidoNaDivisao(){
        Assertions.assertThrows(ArithmeticException.class,() -> {Operacao.divisao(0,2);},"O valor zero é invalido");
    }
}
